import React, { Component } from "react";
import ProductLine from "./components/ProductLine";
import ShippingLine from "./components/ShippingLine";
import HeadingTotalLine from "./components/HeadingTotalLine";
import "./css/tailwind.css";
const catalog = require("./catalog.json");
const _ = require("lodash");
const DEFAULT_STATUS = "Enter the product quantities and shipping";

class App extends Component {
  constructor(props) {
    super(props);

    let quantities = [];
    let quantitiesDisplayed = [];
    let errors = [];
    for (let x = 0; x < catalog.length; x++) {
      quantities.push(0);
      quantitiesDisplayed.push("");
      errors.push(false);
    }

    this.state = {
      quantities: quantities,
      quantitiesDisplayed: quantitiesDisplayed,
      shipping: "0",
      errors: errors,
      status: DEFAULT_STATUS
    };
  }

  handleChange = changeEvent => {
    switch (changeEvent.target.name) {
      case "shipping":
        this.setState({ shipping: changeEvent.target.value });
        break;
      default:
        const tn = changeEvent.target.name.split("-");
        const product = tn[1];
        const updatedQuantityDisplayed = changeEvent.target.value;
        let quantities = this.state.quantities;
        let quantity = quantities[product];
        let quantitiesDisplayed = this.state.quantitiesDisplayed;
        let quantityDisplayed = quantitiesDisplayed[product];
        let status = this.state.status;
        if (updatedQuantityDisplayed.length > 4) {
          status = "Must be 4 characters or less";
        } else {
          quantityDisplayed = updatedQuantityDisplayed;
          quantity = updatedQuantityDisplayed;
          status = DEFAULT_STATUS;
        }
        quantities[product] = quantity;
        quantitiesDisplayed[product] = quantityDisplayed;
        this.setState(
          {
            quantitiesDisplayed: quantitiesDisplayed,
            quantities: quantities,
            status: status
          },
          () => {
            this.validateField("quantity", quantity, product);
          }
        );
        break;
    }
  };

  validateField(fieldName, value, product) {
    let status = this.state.status;
    let errors = this.state.errors;
    let error;

    switch (fieldName) {
      case "quantity":
        error = !value.match("^[0-9]*$");
        if (error) {
          status = "Numbers only";
          errors[product] = true;
        } else {
          errors[product] = false;
        }
        break;
      default:
        break;
    }

    this.setState({ status: status, errors: errors });
  }

  render() {
    let lineItemColor = "box-dark";
    const products = catalog.map(function(product, i) {
      lineItemColor = lineItemColor === "box-dark" ? "box-light" : "box-dark";
      product.lineItemColor = lineItemColor;
      return product;
    });

    let displayTotal;
    const error = _.indexOf(this.state.errors, true);
    if (error >= 0) {
      displayTotal = "";
    } else {
      let total = 0;
      for (let x = 0; x < products.length; x++) {
        let product = products[x];
        let cost = product.price * this.state.quantities[x];
        total += cost;
      }
      total += Number(this.state.shipping);
      displayTotal = total.toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      });
    }

    return (
      <div className="">
        <header className="flex justify-center py-6 bg-grey-lighter">
          <h1 className="font-normal text-green-dark m-0 p-0">Order Form</h1>
        </header>

        <div className="flex justify-center p-4">
          <div className="">{this.state.status}</div>
        </div>

        <div className="flex justify-center mb-12">
          <div className="flex w-5/6 self-center">
            <div className="w-full">
              <HeadingTotalLine
                quantity="Quantity"
                description="Description"
                price="Price"
                cost="Cost"
                key="1"
              />

              {products.map((product, i) => (
                <ProductLine
                  quantity={this.state.quantities[i]}
                  quantityDisplayed={this.state.quantitiesDisplayed[i]}
                  error={this.state.errors[i]}
                  lineItemColor={product.lineItemColor}
                  description={product.description}
                  price={product.price.toFixed(2)}
                  name={"product-" + i}
                  onChange={this.handleChange}
                  key={1 + i}
                />
              ))}

              <ShippingLine
                lineItemColor="box-light"
                quantity=""
                description="Shipping"
                price=""
                cost={this.state.shipping}
                name="shipping"
                value={this.state.shipping}
                options={[
                  {
                    key: "Free",
                    value: "0"
                  },
                  {
                    key: "2 Day",
                    value: "9.99"
                  },
                  {
                    key: "Overnight",
                    value: "24.99"
                  }
                ]}
                onChange={this.handleChange}
                key="100"
              />
              <HeadingTotalLine
                lineItemColor="box-darkest"
                quantity=""
                description="Total"
                price=""
                cost={displayTotal}
                key="101"
              />
            </div>
          </div>
        </div>

        <footer className="flex justify-center mt-4 py-10 bg-grey-lighter">
          <a href="https://bob-humphrey.com">
            <img
              className="w-16"
              src={require("./images/bh-logo-grey.gif")}
              alt="Bob Humphrey website"
            />
          </a>
        </footer>
      </div>
    );
  }
}

export default App;
