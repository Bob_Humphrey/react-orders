import React, { Component } from "react";

class SelectWidget extends Component {
  render() {
    const options = this.props.options;
    return (
      <select
        className={"lg:w-1/5 box " + this.props.lineItemColor}
        name={this.props.name}
        value={this.props.value}
        onChange={this.props.onChange}
      >
        {options.map(function(option, i) {
          return (
            <option key={i} value={option.value}>
              {option.key}
            </option>
          );
        })}
      </select>
    );
  }
}

export default SelectWidget;
