import React, { Component } from "react";
import TextBoxWidget from "../components/TextBoxWidget";
import SelectWidget from "../components/SelectWidget";

class LineItem extends Component {
  render() {
    return (
      <div className="flex justify-center w-full" key={this.props.description}>
        <div className="w-full flex flex-col lg:flex-row">
          {this.props.firstColumn === "TextBoxWidget" && (
            <TextBoxWidget
              lineItemColor={this.props.lineItemColor}
              name={this.props.name}
              quantityDisplayed={this.props.quantityDisplayed}
              onChange={this.props.onChange}
            ></TextBoxWidget>
          )}

          {this.props.firstColumn === "SelectWidget" && (
            <SelectWidget
              options={this.props.options}
              lineItemColor={this.props.lineItemColor}
              name={this.props.name}
              value={this.props.value}
              onChange={this.props.onChange}
            ></SelectWidget>
          )}

          {this.props.firstColumn === "div" && (
            <div className={"lg:w-1/5 box " + this.props.lineItemColor}>
              {this.props.quantity}
            </div>
          )}

          <div className={"lg:w-2/5 box " + this.props.lineItemColor}>
            {this.props.description}
          </div>
          <div
            className={
              "lg:w-1/5 box lg:justify-end " + this.props.lineItemColor
            }
          >
            {this.props.price}
          </div>
          <div
            className={
              "lg:w-1/5 box  lg:justify-end " + this.props.lineItemColor
            }
          >
            {this.props.cost}
          </div>
        </div>
      </div>
    );
  }
}

export default LineItem;
