import React, { Component } from "react";
import LineItem from "../components/LineItem";

class HeadingTotalLine extends Component {
  render() {
    return (
      <LineItem
        lineItemColor="bg-grey-dark text-white"
        quantity={this.props.quantity}
        description={this.props.description}
        price={this.props.price}
        cost={this.props.cost}
        firstColumn="div"
      ></LineItem>
    );
  }
}

export default HeadingTotalLine;
