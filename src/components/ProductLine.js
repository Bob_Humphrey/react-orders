import React, { Component } from "react";
import LineItem from "../components/LineItem";

class ProductLine extends Component {
  render() {
    let cost, displayCost, lineItemColor;

    if (this.props.error === true) {
      displayCost = "";
      lineItemColor = "box-red";
    } else {
      cost = this.props.quantity * this.props.price;
      displayCost = cost.toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      });
      lineItemColor = this.props.lineItemColor;
    }

    return (
      <LineItem
        lineItemColor={lineItemColor}
        description={this.props.description}
        price={this.props.price}
        cost={displayCost}
        name={this.props.name}
        quantity={this.props.quantity}
        quantityDisplayed={this.props.quantityDisplayed}
        firstColumn="TextBoxWidget"
        onChange={this.props.onChange}
      ></LineItem>
    );
  }
}

export default ProductLine;
