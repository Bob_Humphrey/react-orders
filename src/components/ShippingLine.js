import React, { Component } from "react";
import LineItem from "../components/LineItem";

class ShippingLine extends Component {
  render() {
    let cost = Number(this.props.cost);
    let displayCost = cost.toFixed(2);

    return (
      <LineItem
        lineItemColor={this.props.lineItemColor}
        description={this.props.description}
        price={this.props.price}
        cost={displayCost}
        firstColumn="SelectWidget"
        options={this.props.options}
        name={this.props.name}
        value={this.props.value}
        onChange={this.props.onChange}
      ></LineItem>
    );
  }
}

export default ShippingLine;
