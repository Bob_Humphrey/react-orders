import React, { Component } from "react";

class TextBoxWidget extends Component {
  render() {
    return (
      <input
        className={"lg:w-1/5 box " + this.props.lineItemColor}
        type="text"
        name={this.props.name}
        value={this.props.quantityDisplayed}
        onChange={this.props.onChange}
      />
    );
  }
}

export default TextBoxWidget;
